USE [master]
GO
/****** Object:  Database [bookinghotel]    Script Date: 11/30/2023 5:59:42 PM ******/
CREATE DATABASE [bookinghotel]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'bookinghotel', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\bookinghotel.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'bookinghotel_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\bookinghotel_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [bookinghotel] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [bookinghotel].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [bookinghotel] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [bookinghotel] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [bookinghotel] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [bookinghotel] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [bookinghotel] SET ARITHABORT OFF 
GO
ALTER DATABASE [bookinghotel] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [bookinghotel] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [bookinghotel] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [bookinghotel] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [bookinghotel] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [bookinghotel] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [bookinghotel] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [bookinghotel] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [bookinghotel] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [bookinghotel] SET  ENABLE_BROKER 
GO
ALTER DATABASE [bookinghotel] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [bookinghotel] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [bookinghotel] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [bookinghotel] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [bookinghotel] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [bookinghotel] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [bookinghotel] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [bookinghotel] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [bookinghotel] SET  MULTI_USER 
GO
ALTER DATABASE [bookinghotel] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [bookinghotel] SET DB_CHAINING OFF 
GO
ALTER DATABASE [bookinghotel] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [bookinghotel] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [bookinghotel] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [bookinghotel] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [bookinghotel] SET QUERY_STORE = OFF
GO
USE [bookinghotel]
GO
/****** Object:  Table [dbo].[Booking]    Script Date: 11/30/2023 5:59:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Booking](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[from_date] [datetime] NULL,
	[to_date] [datetime] NULL,
	[room_id] [int] NULL,
	[price] [float] NULL,
	[fullName] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[notes] [nvarchar](50) NULL,
	[number_people] [int] NULL,
	[user_id] [int] NULL,
	[status] [bit] NULL,
 CONSTRAINT [PK_Booking] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Hotel]    Script Date: 11/30/2023 5:59:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Hotel](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[ward] [nvarchar](50) NULL,
	[address] [nvarchar](50) NULL,
	[description] [ntext] NULL,
	[star] [int] NULL,
	[province] [nvarchar](50) NULL,
	[banner] [ntext] NULL,
 CONSTRAINT [PK_Hotel] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Room]    Script Date: 11/30/2023 5:59:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Room](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](50) NULL,
	[price] [float] NULL,
	[sale_price] [float] NULL,
	[capacity] [int] NULL,
	[number_bed] [int] NULL,
	[square] [int] NULL,
	[hotel_id] [int] NULL,
	[desci] [ntext] NULL,
 CONSTRAINT [PK_Room] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 11/30/2023 5:59:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [nvarchar](50) NULL,
	[email] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[address] [nvarchar](50) NULL,
	[gender] [bit] NULL,
	[phone] [nchar](11) NULL,
	[role_id] [int] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Booking] ON 

INSERT [dbo].[Booking] ([id], [from_date], [to_date], [room_id], [price], [fullName], [email], [notes], [number_people], [user_id], [status]) VALUES (2, CAST(N'2023-07-01T00:00:00.000' AS DateTime), CAST(N'2023-07-03T00:00:00.000' AS DateTime), 4, 11111, N'ai', N'sonngy@gmail.com', N'ggggg', 2, 3, 0)
SET IDENTITY_INSERT [dbo].[Booking] OFF
GO
SET IDENTITY_INSERT [dbo].[Hotel] ON 

INSERT [dbo].[Hotel] ([id], [name], [ward], [address], [description], [star], [province], [banner]) VALUES (1, N'Shin''s Apartment', N'Vũng Tàu', N'28 Đường Thi Sách, Vung Tau, Vietnam', N'You''re eligible for a Genius discount at Shin''s Apartment 1 - The Sóng Vũng Tàu! To save at this property, all you have to do is sign in.Shin''s Apartment - The Sóng Vũng Tàu is a sustainable apartment in Vung Tau, offering free WiFi, private beach area and infinity pool. Guests can take in the sea views and spend some time on the beach. Featuring a lift, this property also provides guests with a water park. The apartment features private parking, a sauna and a 24-hour front desk.The units in the apartment complex are equipped with air conditioning, a seating area, a flat-screen TV with cable channels, a kitchen, a dining area, a safety deposit box and a private bathroom with a walk-in shower, slippers and a hair dryer. A terrace with an outdoor dining area and mountain views is offered in all units. At the apartment complex, all units come with bed linen and towels.There is a coffee shop, and a grocery delivery service and a minimarket are also available.You can play table tennis at the apartment, and car hire is available. A baby safety gate is also available at Shin''s Apartment - The Sóng Vũng Tàu, while guests can also relax in the garden.Back Beach is less than 1 km from the accommodation, while Ho May Culture and Ecotourism Park is 3.7 km away. The nearest airport is Vung Tau Airport, 3 km from Shin''s Apartment - The Sóng Vũng Tàu.Distance in property description is calculated using © OpenStreetMap', 4, N'Bà Rịa', N'https://cf.bstatic.com/xdata/images/hotel/max1024x768/462965469.jpg?k=2f811fcbd3864f340c336f58444dc96bf641c3c47b45c62004ab60965681f31d&o=&hp=1')
INSERT [dbo].[Hotel] ([id], [name], [ward], [address], [description], [star], [province], [banner]) VALUES (2, N'LH APARTMENT DA NANG', N'Đà Nẵng', N'43 K 82 Nguyễn Duy Hiệu, Da Nang, Vietnam', N'You''re eligible for a Genius discount at LH APARTMENT DA NANG! To save at this property, all you have to do is sign in.

1.2 km from My Khe Beach, LH APARTMENT DA NANG is a sustainable property located in Da Nang and provides air-conditioned rooms with free WiFi and parking on-site. The property features city views and is 2.1 km from Bac My An Beach and 1.7 km from Love Lock Bridge Da Nang. The accommodation features a lift, full-day security and luggage storage for guests.

The aparthotel provides guests with a balcony, sea views, a seating area, a flat-screen TV, a fully equipped kitchen with a fridge and a stovetop, and a private bathroom with walk-in shower and slippers. At the aparthotel, all units are soundproof. The units will provide guests with a wardrobe and a kettle.

Guests can also relax in the shared lounge area.

Cham Museum is 2.6 km from the aparthotel, while Song Han Bridge is 3 km away. The nearest airport is Da Nang International Airport, 5 km from LH APARTMENT DA NANG.

Distance in property description is calculated using © OpenStreetMap', 3, N'Đà Nẵng', N'https://cf.bstatic.com/xdata/images/hotel/max1024x768/377229542.jpg?k=59d3bdd33d7b052d2743179ccbae89f4c6a212bda90b6515dd44e22fa4904d0e&o=&hp=1')
INSERT [dbo].[Hotel] ([id], [name], [ward], [address], [description], [star], [province], [banner]) VALUES (3, N'Homestay Góc Nhà Nhỏ', N'Đà Lạt', N' Đường Nguyễn Trung Trực, Da Lat, Vietnam', N'Located in Da Lat in the Lam Dong region, Homestay Góc Nhà Nhỏ has a balcony. This property offers access to a terrace, free private parking and free WiFi. The property is non-smoking and is situated 3.7 km from Truc Lam Temple.

The spacious apartment is composed of 3 bedrooms, a living room, a fully equipped kitchen, and 2 bathrooms. A flat-screen TV is offered.

Tuyen Lam Lake is 3.8 km from the apartment, while Lam Vien Square is 3.9 km from the property. The nearest airport is Lien Khuong Airport, 27 km from Homestay Góc Nhà Nhỏ.

Distance in property description is calculated using © OpenStreetMap', 4, N'Lâm Đồng ', N'https://cf.bstatic.com/xdata/images/hotel/max1024x768/456982376.jpg?k=597bbfd88ef3d02ac1535aeca474a84e3d316f1c3554780e6783390f8d3fc401&o=&hp=1')
INSERT [dbo].[Hotel] ([id], [name], [ward], [address], [description], [star], [province], [banner]) VALUES (12, N'Hotel the World', N'Hà Nội', N'Cầu Giấy, Hà Nội', N'Đẹp', 2, N'Hà Nội', N'https://th.bing.com/th/id/OIP.fxF9284MPofVA2O7ilODtQHaE8?w=239&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7')
INSERT [dbo].[Hotel] ([id], [name], [ward], [address], [description], [star], [province], [banner]) VALUES (15, N'Hotel Nha Trang', N'Nha Trang', N'Nha Trang', N'Rất đẹp', 2, N'Nha Trang', N'https://www.bing.com/th?id=OADD2.7421765055757_1HFHO34A3B80PET4XM&pid=21.2&c=16&roil=0&roit=0&roir=1&roib=1&w=472&h=247&rs=0')
SET IDENTITY_INSERT [dbo].[Hotel] OFF
GO
SET IDENTITY_INSERT [dbo].[Room] ON 

INSERT [dbo].[Room] ([id], [type], [price], [sale_price], [capacity], [number_bed], [square], [hotel_id], [desci]) VALUES (1, N'Single room', 1200000, 1000000, 2, 1, 20, 1, N'Phòng đơn')
INSERT [dbo].[Room] ([id], [type], [price], [sale_price], [capacity], [number_bed], [square], [hotel_id], [desci]) VALUES (2, N'Twin room', 2400000, 2000000, 4, 2, 40, 1, N'Phòng đôi')
INSERT [dbo].[Room] ([id], [type], [price], [sale_price], [capacity], [number_bed], [square], [hotel_id], [desci]) VALUES (4, N'Single room', 1300000, 1100000, 2, 1, 30, 2, N'Phòng đơn')
INSERT [dbo].[Room] ([id], [type], [price], [sale_price], [capacity], [number_bed], [square], [hotel_id], [desci]) VALUES (5, N'Family room', 5000000, 4000000, 5, 2, 50, 3, N'Phòng gia đình')
INSERT [dbo].[Room] ([id], [type], [price], [sale_price], [capacity], [number_bed], [square], [hotel_id], [desci]) VALUES (8, N'Double', 1540, 1256, 12, 1, 68, 3, N'You''re eligible for a Genius discount at Shin''s Apartment 1 - The Sóng Vũng Tàu! To save at this property, all you have to do is sign in.Shin''s Apartment - The Sóng Vũng Tàu is a sustainable apartment in Vung Tau, offering free WiFi, private beach area and infinity pool. Guests can take in the sea views and spend some time on the beach. Featuring a lift, this property also provides guests with a water park. The apartment features private parking, a sauna and a 24-hour front desk.The units in the apartment complex are equipped with air conditioning, a seating area, a flat-screen TV with cable channels, a kitchen, a dining area, a safety deposit box and a private bathroom with a walk-in shower, slippers and a hair dryer. A terrace with an outdoor dining area and mountain views is offered in all units. At the apartment complex, all units come with bed linen and towels.There is a coffee shop, and a grocery delivery service and a minimarket are also available.You can play table tennis at the apartment, and car hir')
INSERT [dbo].[Room] ([id], [type], [price], [sale_price], [capacity], [number_bed], [square], [hotel_id], [desci]) VALUES (15, N'DInh kout', 59788, 33344, 5, 2, 50, 2, N'dinh cao lăm')
INSERT [dbo].[Room] ([id], [type], [price], [sale_price], [capacity], [number_bed], [square], [hotel_id], [desci]) VALUES (22, N'Double Room', 2000000, 1999999, 5, 3, 80, 12, N'đẹp quá trời')
INSERT [dbo].[Room] ([id], [type], [price], [sale_price], [capacity], [number_bed], [square], [hotel_id], [desci]) VALUES (23, N'Deluxe', 6000000, 4000000, 2, 1, 50, 1, N'Phòng đơn')
INSERT [dbo].[Room] ([id], [type], [price], [sale_price], [capacity], [number_bed], [square], [hotel_id], [desci]) VALUES (24, N'Double Room', 1290000, 1250000, 5, 2, 68, 15, N'Đẹp')
SET IDENTITY_INSERT [dbo].[Room] OFF
GO
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([user_id], [user_name], [email], [password], [address], [gender], [phone], [role_id]) VALUES (5, N'son', N'nguyenvan1102lt@gmail.com', N'4297f44b13955235245b2497399d7a93', N'ha noi', 1, N'0986328228 ', 2)
INSERT [dbo].[Users] ([user_id], [user_name], [email], [password], [address], [gender], [phone], [role_id]) VALUES (8, N'anh', N'sonnguyen2002lt@gmail.com', N'4297f44b13955235245b2497399d7a93', N'ha noi', 1, N'0827338730 ', 1)
INSERT [dbo].[Users] ([user_id], [user_name], [email], [password], [address], [gender], [phone], [role_id]) VALUES (10, N'anh you', N'anhyou@gmail.com', N'4297f44b13955235245b2497399d7a93', N'Thạch Thất', 1, N'0973827389 ', 1)
INSERT [dbo].[Users] ([user_id], [user_name], [email], [password], [address], [gender], [phone], [role_id]) VALUES (11, N'hanhkh', N'hanhksnhc@gmail.com', N'4297f44b13955235245b2497399d7a93', N'Việt Nam', 0, N'0386289374 ', 1)
INSERT [dbo].[Users] ([user_id], [user_name], [email], [password], [address], [gender], [phone], [role_id]) VALUES (12, N'khach hang', N'khachhang@gmail.com', N'4297f44b13955235245b2497399d7a93', N'Lại Thượng', 1, N'0397253726 ', 1)
INSERT [dbo].[Users] ([user_id], [user_name], [email], [password], [address], [gender], [phone], [role_id]) VALUES (13, N'dung cai nit', N'dungcainit@gmail.com', N'4297f44b13955235245b2497399d7a93', N'Việt Nam', 0, N'0397749932 ', 1)
INSERT [dbo].[Users] ([user_id], [user_name], [email], [password], [address], [gender], [phone], [role_id]) VALUES (14, N'son', N'son1102@gmail.com', N'25f9e794323b453885f5181f1b624d0b', N'Lại Thượng', 1, N'0397253726 ', 1)
SET IDENTITY_INSERT [dbo].[Users] OFF
GO
ALTER TABLE [dbo].[Booking] ADD  CONSTRAINT [DF_Booking_status]  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[Booking]  WITH CHECK ADD  CONSTRAINT [FK_Booking_Room] FOREIGN KEY([room_id])
REFERENCES [dbo].[Room] ([id])
GO
ALTER TABLE [dbo].[Booking] CHECK CONSTRAINT [FK_Booking_Room]
GO
ALTER TABLE [dbo].[Room]  WITH CHECK ADD  CONSTRAINT [FK_Room_Hotel] FOREIGN KEY([hotel_id])
REFERENCES [dbo].[Hotel] ([id])
GO
ALTER TABLE [dbo].[Room] CHECK CONSTRAINT [FK_Room_Hotel]
GO
USE [master]
GO
ALTER DATABASE [bookinghotel] SET  READ_WRITE 
GO
