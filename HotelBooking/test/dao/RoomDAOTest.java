/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package dao;

import java.sql.SQLException;
import java.util.ArrayList;
import model.Room;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sonng
 */
public class RoomDAOTest {

    public RoomDAOTest() {
    }

    

    @Test
    public void testGetRooms1() {
        RoomDAO dao = new RoomDAO();
        ArrayList<Room> result = dao.getRooms();
        assertFalse(result.isEmpty());
    }

    @Test
    public void testGetRooms2() {
        RoomDAO dao = new RoomDAO();
        ArrayList<Room> result = dao.getRooms();
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(9, result.size());
    }

    @Test
    public void testGetRooms3() {
        RoomDAO dao = new RoomDAO();
        ArrayList<Room> result = dao.getRooms();
        assertEquals("Expecting 12 rooms, but found " + result.size(), 12, result.size()); 
    }

}
