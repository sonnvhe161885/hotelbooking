/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.*;

/**
 *
 * @author Admin
 */
public class HotelDAO extends DBContext {

    public ArrayList<Hotel> getHotel(String checkin, String checkout, String address, String guest, String star,String sort, int index, int pagesize) {
        ArrayList<Hotel> list = new ArrayList<>();

        String sql = "   select *, \n"
                + "  (select max([price]) as highestPri from Room where hotel_id = h.id) as highestPrice,\n"//tìm giá phòng lớn nhất
                + "  (select min([price]) as lowestPri from Room where hotel_id = h.id) as lowestPrice\n"
                + "  from Hotel h   where  1=1 ";//đk có sẵn để cắt câu lệnh bắt đầu bằng and
        if (!star.isEmpty()) {
            sql += "and  h.star = " + star;
        }
            sql += "and h.ward like N'%" + address + "%' ";//tìm theo ward
        sql += " and h.id in (select r.hotel_id from Room r where  capacity >= " + guest + " and \n"
                + "  r.id not in "//not in đều ko đc lấy vào 
                + "( select b.room_id from Booking b where b.from_date between '" + checkin + "' and '" + checkout +
                "' or b.to_date  between '" + checkin + "' and '" + checkout + "' and b.status=1))";//check xem phòng nào bạn
        sql += "  order by highestPrice "+ sort +",  lowestPrice   " + sort
                + "  OFFSET  " + (index - 1) * pagesize + " ROWS FETCH NEXT  " + pagesize + "  ROWS ONLY";//phân trang
        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Hotel h = new Hotel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getInt(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10));
                list.add(h);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
        public ArrayList<Hotel> getHotelDESC(String checkin, String checkout, String address, String guest, String star,String sort, int index, int pagesize) {
        ArrayList<Hotel> list = new ArrayList<>();

        String sql = "   select *, \n"
                + "  (select min([price]) as highestPri from Room where hotel_id = h.id) as highestPrice,\n"//tìm giá phòng lớn nhất
                + "  (select max([price]) as lowestPri from Room where hotel_id = h.id) as lowestPrice\n"
                + "  from Hotel h   where  1=1 ";//đk có sẵn để cắt câu lệnh bắt đầu bằng and
        if (!star.isEmpty()) {
            sql += "and  h.star = " + star;
        }
            sql += "and h.ward like N'%" + address + "%' ";//tìm theo ward
        sql += " and h.id in (select r.hotel_id from Room r where  capacity >= " + guest + " and \n"
                + "  r.id not in "//not in đều ko đc lấy vào 
                + "( select b.room_id from Booking b where b.from_date between '" + checkin + "' and '" + checkout +
                "' or b.to_date  between '" + checkin + "' and '" + checkout + "' and b.status=1))";//check xem phòng nào bạn
        sql += "  order by highestPrice ,  lowestPrice   " + sort
                + "  OFFSET  " + (index - 1) * pagesize + " ROWS FETCH NEXT  " + pagesize + "  ROWS ONLY";//phân trang
        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Hotel h = new Hotel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getInt(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10));
                list.add(h);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
    public ArrayList<Hotel> getHotelASC(String checkin, String checkout, String address, String guest, String star, int index, int pagesize) {
        ArrayList<Hotel> list = new ArrayList<>();

        String sql = "   select *, \n"
                + "  (select max([price]) as highestPri from Room where hotel_id = h.id) as highestPrice,\n"//tìm giá phòng lớn nhất
                + "  (select min([price]) as lowestPri from Room where hotel_id = h.id) as lowestPrice\n"
                + "  from Hotel h   where  1=1 ";//đk có sẵn để cắt câu lệnh bắt đầu bằng and
        if (!star.isEmpty()) {
            sql += "and  h.star = " + star;
        }
            sql += "and h.ward like N'%" + address + "%' ";//tìm theo ward
        sql += " and h.id in (select r.hotel_id from Room r where  capacity >= " + guest + " and \n"
                + "  r.id not in "//not in đều ko đc lấy vào 
                + "( select b.room_id from Booking b where b.from_date between '" + checkin + "' and '" + checkout +
                "' or b.to_date  between '" + checkin + "' and '" + checkout + "' and b.status=1))";//check xem phòng nào bạn
        sql += "  order by h.id  "
                + "  OFFSET  " + (index - 1) * pagesize + " ROWS FETCH NEXT  " + pagesize + "  ROWS ONLY";//phân trang
        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Hotel h = new Hotel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getInt(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10));
                list.add(h);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public Hotel getHotelById(int id, String checkin, String checkout) {
        String sql = "   select *, \n"
                + "  (select max([price]) as highestPri from Room where hotel_id = h.id) as highestPrice,\n"
                + "  (select min([price]) as lowestPri from Room where hotel_id = h.id) as lowestPrice\n"
                + "  from Hotel h   where h.id = " + id;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Hotel h = new Hotel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getInt(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10));
                ArrayList<Room> rl = new RoomDAO().getRoomByHotelId(id, checkin, checkout);//get nhưng phòng ko ai đặt
                h.setRooms(rl);

                return h;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Hotel getHotelById(int id) {
        String sql = "   select *, \n"
                + "  (select max([price]) as highestPri from Room where hotel_id = h.id) as highestPrice,\n"
                + "  (select min([price]) as lowestPri from Room where hotel_id = h.id) as lowestPrice\n"
                + "  from Hotel h   where h.id = " + id;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Hotel h = new Hotel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getInt(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10));
                return h;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public ArrayList<Hotel> getHotels() {
        ArrayList<Hotel> list = new ArrayList<>();

        String sql = "   select *, \n"
                + "  (select max([price]) as highestPri from Room where hotel_id = h.id) as highestPrice,\n"
                + "  (select min([price]) as lowestPri from Room where hotel_id = h.id) as lowestPrice\n"
                + "  from Hotel h   where  1=1 ";
        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Hotel h = new Hotel(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
                        rs.getString(5), rs.getInt(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10));
                list.add(h);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void updateHotel(String name, String ward, String address, String description, String star, String province, String banner, String id) {
        try {
            String sql = "update [Hotel] set  [name] =?  ,[ward]  =? ,[address]  =?,[description] =?  ,[star]   =? ,[province]  =? ,[banner]  =?\n"
                    + "   where id = " + id;

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, name);
            stm.setString(2, ward);
            stm.setString(3, address);
            stm.setString(4, description);
            stm.setString(5, star);
            stm.setString(6, province);
            stm.setString(7, banner);
            stm.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addHotel(String name, String ward, String address, String description, String star, String province, String banner) {
        try {
            String sql = "INSERT INTO [dbo].[Hotel]   ([name]   ,[ward]  ,[address] ,[description]  ,[star]   ,[province]  ,[banner])\n"
                    + "     VALUES  (?,?,?,?,?,?,?)";

            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, name);
            stm.setString(2, ward);
            stm.setString(3, address);
            stm.setString(4, description);
            stm.setString(5, star);
            stm.setString(6, province);
            stm.setString(7, banner);
            stm.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteHotel(String id) {
        try {
            String sql = "delete [Hotel] where id = " + id;
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
