/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Room;

/**
 *
 * @author Admin
 */
public class RoomDAO extends DBContext {

    public ArrayList<Room> getRoomByHotelId(int hid, String checkin, String checkout) {
        ArrayList<Room> list = new ArrayList<>();
        String sql = "   	select * from Room r where r.hotel_id = " + hid
                + "	and r.id not in ( select b.room_id from Booking b where b.from_date between '" + checkin + "' and '" + checkout + "' or b.to_date  between '" + checkin + "' and '" + checkout + "')";
        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Room(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4),
                        rs.getInt(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getString(9)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public ArrayList<Room> getRooms() {
        ArrayList<Room> list = new ArrayList<>();
        String sql = " select * from Room ";
        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Room(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4),
                        rs.getInt(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getString(9)));
            }
        } catch (Exception e) {
        }
        return list;
    }
    public Room getRoomById(String id) {
        String sql = " select * from Room  where id = " + id;
        System.out.println(sql);
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Room(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getDouble(4),
                        rs.getInt(5), rs.getInt(6), rs.getInt(7), rs.getInt(8), rs.getString(9), new HotelDAO().getHotelById(rs.getInt(8)));
                
            }
        } catch (Exception e) {
        }
        return null;
    }
    public void updateRoom(String type, String price, String sale_price, String capacity, String number_bed, String square, String hotel_id, String desci, String id) {
        try {
            String sql = "update [Room]  set[type] =?  ,[price] =? ,[sale_price]  =?  ,[capacity]  =?,[number_bed]   =? ,[square] =? ,[hotel_id]  =?,[desci]  =?\n"
                    + "  where id = " + id;
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, type);
            stm.setString(2, price);
            stm.setString(3, sale_price);
            stm.setString(4, capacity);
            stm.setString(5, number_bed);
            stm.setString(6, square);
            stm.setString(7, hotel_id);
            stm.setString(8, desci);
            stm.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addRoom(String type, String price, String sale_price, String capacity, String number_bed, String square, String hotel_id, String desci) {
        try {
            String sql = "INSERT INTO [dbo].[Room] ([type]   ,[price] ,[sale_price]   ,[capacity] ,[number_bed]   ,[square] ,[hotel_id] ,[desci])\n"
                    + "     VALUES (?,?,?,?,?,?,?,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, type);
            stm.setString(2, price);
            stm.setString(3, sale_price);
            stm.setString(4, capacity);
            stm.setString(5, number_bed);
            stm.setString(6, square);
            stm.setString(7, hotel_id);
            stm.setString(8, desci);
            stm.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void deleteRoom(String id) {
        try {
            String sql = "delete [Room] where id = " + id;
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
