/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Booking;
import model.Room;

/**
 *
 * @author Admin
 */
public class BookingDAO extends DBContext {

    public void bookRoom(String checkin, String checkout, String price, String rid, String guest, String fullname, String email, String notes, String uid) {
        try {
            String sql = "INSERT INTO [dbo].[Booking]\n"
                    + "           ([from_date]    ,[to_date] ,[room_id]   ,[price]  ,[fullName] ,[email]  ,[notes],[number_people],[user_id])\n"
                    + "     VALUES (?,?,?,?,?,?,?,?,?)";

            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setString(1, checkin);
            pstm.setString(2, checkout);
            pstm.setString(3, rid);
            pstm.setString(4, price);
            pstm.setString(5, fullname);
            pstm.setString(6, email);
            pstm.setString(7, notes);
            pstm.setString(8, guest);
            pstm.setString(9, uid);
            pstm.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Booking> getAllOrderByuId(int uid) {
        ArrayList<Booking> ol = new ArrayList<>();
        String sql = "   select * from Booking  where  user_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, uid);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ol.add(new Booking(rs.getInt(1), rs.getDate(2), rs.getDate(3), rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getInt(10)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ol;
    }

    public ArrayList<Booking> getAllOrders() {
        ArrayList<Booking> ol = new ArrayList<>();
        String sql = "   select * from Booking  ";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Booking b = new Booking(rs.getInt(1), rs.getDate(2), rs.getDate(3), rs.getInt(4), rs.getInt(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getInt(9), rs.getInt(10));
//            b.setUser( new UserDAO().getUserById(rs.getInt("user_id")));
//            b.setRoom(new RoomDAO().getRoomById(rs.getString("room_id")));
                b.setStatus(rs.getBoolean("status"));
                ol.add(b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ol;
    }

    public void changeStautus(String id, String status) {
        try {
            String sql = "update  [Booking] set  status =" + status + " where id = " + id;
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
