/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Admin
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Hotel {

    int id;
    String name;
    String ward;
    String address;
    String description;
    int star;
    String province;
    String banner;
    String highestPrice;
    String lowestPrice;
    ArrayList<Room> rooms;

    public Hotel(int id, String name, String ward, String address, String description, int star, String province, String banner, String highestPrice, String lowestPrice) {
        this.id = id;
        this.name = name;
        this.ward = ward;
        this.address = address;
        this.description = description;
        this.star = star;
        this.province = province;
        this.banner = banner;
        this.highestPrice = highestPrice;
        this.lowestPrice = lowestPrice;
    }
    
    
}
