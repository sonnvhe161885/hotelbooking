/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Admin
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Booking {

    int id;
    Date from_date;
    Date to_date;
    int room_id;
    double price;
    String fullName;
    String email;
    String notes;
    int number_people;
    int user_id;
    User user;
    Room room;
    boolean status;

    public Booking(int id, Date from_date, Date to_date, int room_id, double price, String fullName, String email, String notes, int number_people, int user_id) {
        this.id = id;
        this.from_date = from_date;
        this.to_date = to_date;
        this.room_id = room_id;
        this.price = price;
        this.fullName = fullName;
        this.email = email;
        this.notes = notes;
        this.number_people = number_people;
        this.user_id = user_id;
    }
    
}
