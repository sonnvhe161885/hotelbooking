/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author Admin
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Room {

    int id;
    String type;
    double price;
    double sale_price;
    int capacity;
    int number_bed;
    int square;
    int hotel_id;
    String desci;
    Hotel hotel;

    public Room(int id, String type, double price, double sale_price, int capacity, int number_bed, int square, int hotel_id, String desci) {
        this.id = id;
        this.type = type;
        this.price = price;
        this.sale_price = sale_price;
        this.capacity = capacity;
        this.number_bed = number_bed;
        this.square = square;
        this.hotel_id = hotel_id;
        this.desci = desci;
    }

}
