/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HotelDAO;
import dao.RoomDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class RoomManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RoomManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RoomManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action") == null ? "list" : request.getParameter("action");

        RoomDAO dao = new RoomDAO();
        switch (action) {
            case "add":
                request.setAttribute("hl", new HotelDAO().getHotels());
                request.getRequestDispatcher("roomadd.jsp").forward(request, response);
                break;
            case "delete":
                dao.deleteRoom(request.getParameter("rid"));
                response.sendRedirect("RoomManager");
                break;
            case "edit":
                request.setAttribute("p", dao.getRoomById(request.getParameter("rid")));
                request.setAttribute("hl", new HotelDAO().getHotels());
                request.getRequestDispatcher("roomedit.jsp").forward(request, response);
                break;
            default:
                request.setAttribute("rl", dao.getRooms());
                request.getRequestDispatcher("roomlist.jsp").forward(request, response);
                break;
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action") == null ? "list" : request.getParameter("action");
        RoomDAO dao = new RoomDAO();
           String id = request.getParameter("id");
           String type = request.getParameter("type");
           String price = request.getParameter("price");
           String saleprice = request.getParameter("saleprice");
           String capacity = request.getParameter("capacity");
           String numberbed = request.getParameter("numberbed");
           String square = request.getParameter("square");
           String descri = request.getParameter("descri");
           String hid = request.getParameter("hid");
        switch (action) {
            case "add":
                dao.addRoom(type, price, saleprice, capacity, numberbed, square, hid, descri);
                response.sendRedirect("RoomManager");
                break;
            case "edit":
                dao.updateRoom(type, price, saleprice, capacity, numberbed, square, hid, descri, id);
                response.sendRedirect("RoomManager");
                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
