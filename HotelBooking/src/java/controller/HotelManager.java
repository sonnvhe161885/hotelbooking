/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HotelDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class HotelManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HotelManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HotelManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action") == null ? "list" : request.getParameter("action");
        HotelDAO dao = new HotelDAO();
        switch (action) {
            case "delete":
                dao.deleteHotel(request.getParameter("hid"));
                response.sendRedirect("HotelManager");
                break;
            case "edit":
                request.setAttribute("p", dao.getHotelById(Integer.valueOf(request.getParameter("hid"))));
                request.getRequestDispatcher("hoteledit.jsp").forward(request, response);
                break;
            case "add":
                request.getRequestDispatcher("hoteladd.jsp").forward(request, response);
                break;
            default:
                request.setAttribute("hl", dao.getHotels());
                request.getRequestDispatcher("hotellist.jsp").forward(request, response);
                break;
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action") == null ? "list" : request.getParameter("action");
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String ward = request.getParameter("ward");
        String province = request.getParameter("province");
        String address = request.getParameter("address");
        String star = request.getParameter("star");
        String descri = request.getParameter("descri");
        String img = request.getParameter("img");
        HotelDAO dao = new HotelDAO();
        switch (action) {
            case "add":
                dao.addHotel(name, ward, address, descri, star, province, img);
                response.sendRedirect("HotelManager");
                break;
            case "edit":
                dao.updateHotel(name, ward, address, descri, star, province, img, id);
                response.sendRedirect("HotelManager");
                break;

        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
