
<nav class="navbar navbar-default">
    <div class="container">

        <!--         BRAND 
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#alignment-example" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                    </button>
                    <a class="navbar-brand" href="HomePage">Online Shop</a>
                </div>-->
        <!-- COLLAPSIBLE NAVBAR -->
        <div class="collapse navbar-collapse" id="alignment-example">

            <!-- Links -->
            <ul class="nav navbar-nav">
                <li >
                    <a style="text-decoration: none;"  style="font-size: 20px;" href="ManagerUser"> Manage User</a>
                </li>
                <li >
                    <a style="text-decoration: none;" href="HotelManager"> Manage Hotel</a>
                </li>
                <li >
                    <a style="text-decoration: none;" href="RoomManager"> Manage Room</a>
                </li>
                <li >
                    <a style="text-decoration: none;" href="BookingManager"> Manage Booking</a>
                </li>
                <li >
                    <a style="text-decoration: none;" href="HomePage">Back</a>
                </li>
            </ul>
        </div>

    </div>
</nav>