<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Hotel</title>

        <link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/nice-select.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main-color.css">


    </head>

    <body class="biolife-body">

        <jsp:include page="header.jsp"/>

        <!-- Page Contain -->
        <div class="page-contain">
            <div id="main-content" class="main-content container">
                <h1>Detail information about hotel</h1>
                <div style="margin-bottom: 10px;border: 1px solid lightskyblue; background-color:#87cefa5e ; border-radius: 10px;display: flex; justify-content: space-between;padding: 8px; " >
                    <img width="30%" style="height: 225px;object-fit: cover;"  src="${h.banner}" alt="alt"/>
                    <div width="35%">
                        <h2 style="color: #636ce4">${h.name}</h2>
                        <p>Address: ${h.address}</p>
                        <p><i class="fa fa-bar-chart" aria-hidden="true"></i> Best price</p>
                        <p><i class="fa fa-car" aria-hidden="true"></i> Free taking</p>
                        <p style="color: green; font-size: 11px">FREE cancellation<br>
                            No prepayment needed – pay at the property</p>
                    </div>
                    <div width="33%">
                        <h3>Range Price:<br> ${h.lowestPrice}vnd - ${h.highestPrice}vnd</h3>
                        <p>Includes taxes and charges</p>
                    </div>
                </div>
                <h2>Available Rooms in hotel</h2>
                <!--                <form method="post" action="DetailHotel">
                                    <select name="sort1" class="form-select" style="width: 10%">
                                    <option value="asc">Giá từ thấp đến cao</option>
                                    <option value="desc">Giá từ cao đến thấp</option>
                                </select>
                                    <button type="submit" class="btn btn-primary">Find</button>
                                </form>-->

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Room type</th>
                            <th>Number of guests</th>             
                            <th>Today's price</th>
                            <th>Square</th>
                            <th>Description</th>
                            <th>Booking</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="r" items="${h.rooms}">
                            <tr>
                                <td> <h4>${r.type}</h4> <br> ${r.number_bed} double bed <i class="fa fa-bed" aria-hidden="true"></i>
                                    <br>  Air conditioning <i class="fa fa-snowflake-o" aria-hidden="true"></i>
                                    <br>  Flat-screen TV   <i class="fa fa-television" aria-hidden="true"></i>
                                    <br>  Free WiFi <i class="fa fa-wifi" aria-hidden="true"></i>

                                </td>

                                <td>${r.capacity} <i class="fa fa-user" aria-hidden="true"></i></td>
                                <td> <br><del style="color: red">${r.price} vnd</del> 
                                    <br> ${r.sale_price} vnd</td>
                                <td>${r.square} m<sup>2</sup></td>
                                <td>${r.desci}</td>
                                <td><a href="BookingRoom?rid=${r.id}&hid=${h.id}&checkin=${param['checkin']}&checkout=${param['checkout']}&guest=${param['guest']}&rtype=${r.type}&price=${r.sale_price}" class="btn btn-primary">Booking</a></td>
                            </tr>
                        </c:forEach>

                    </tbody>
                </table>

            </div>
        </div>
        <jsp:include page="footer.jsp"/>



        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>

        <script src="assets/js/jquery-3.4.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.countdown.min.js"></script>
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <script src="assets/js/jquery.nicescroll.min.js"></script>
        <script src="assets/js/slick.min.js"></script>
        <script src="assets/js/biolife.framework.js"></script>
        <script src="assets/js/functions.js"></script>
    </body>

</html>