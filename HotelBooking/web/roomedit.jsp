<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Hotel</title>

        <link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/slick.min.css">
        <!--<link rel="stylesheet" href="assets/css/style.css">-->
        <link rel="stylesheet" href="assets/css/main-color.css">

    </head>

    <body class="biolife-body">
        <jsp:include page="headeradmin.jsp"/>

        <!-- Main content -->
        <div id="main-content" class="main-content">
            <div class="container-fluid">

                <div class="row" >

                    <div class="col-md-12">
                        <h1 style="text-align: center;">Add Room</h1>
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <form action="RoomManager?action=add" method="post">
                                    <div class="modal-body">
                                        <b>ID: </b><input required="" type="text" class="form-control" name="id" value="${p.id}" readonly=""><br>
                                        <b>Type: </b><input required="" type="text" class="form-control" value="${p.type}" name="type"><br>
                                        <b>Price: </b><input required="" type="text" class="form-control" value="${p.price}" name="price"><br>
                                        <b>Sale Price: </b><input required="" type="text" class="form-control" value="${p.sale_price}" name="saleprice"><br>
                                        <b>Capacity: </b><input required="" type="number" step="1" min="1" class="form-control" value="${p.capacity}" name="capacity"><br>
                                        <b>Number bed: </b><input required="" type="number" step="1" min="1" class="form-control" value="${p.number_bed}" name="numberbed"><br>
                                        <b>Square: </b><input required="" type="number" step="1" min="1" class="form-control" value="${p.square}" name="square"><br>
                                        <b>Description: </b><input required="" type="text" class="form-control" value="${p.desci}" name="descri"><br>
                                        <select name="hid">
                                            <c:forEach var="h" items="${hl}">
                                                <option value="${h.id}" ${p.hotel_id==h.id?"selected":""}>${h.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-success" value="submit">Submit</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
        <script>
            $(document).ready(function () {
                $("#slidetable").DataTable();
            });
        </script>
        <script >
            function changeImg(x) {
                document.getElementById("image").src = x.value;
            }
        </script>
        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <script src="assets/js/slick.min.js"></script>
        <script src="assets/js/biolife.framework.js"></script>
        <script src="assets/js/functions.js"></script>
    </body>

</html>