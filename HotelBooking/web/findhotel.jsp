<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Hotel</title>

        <link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/nice-select.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main-color.css">


    </head>

    <body class="biolife-body">

        <jsp:include page="header.jsp"/>

        <!-- Page Contain -->
        <div class="page-contain">
            <div id="main-content" class="main-content container">
                <img src="https://img.freepik.com/premium-psd/traveling-social-media-banner-facebook-cover-template-premium-psd_632100-210.jpg" width="100%"  alt="alt"/>
                <form action="FindHotel" style="display: flex; justify-content: space-between; " method="">
                    <!--<input type="text" placeholder="Address" name="pro" style="width: 20%" class="form-control"> comment -->
                    <select name="pro" class="form-select" style="width: 10%">
                        <option value="">Address</option>
                        <option value="Hà Nội">Hà Nội</option>
                        <option value="Đà Nẵng">Đà Nẵng</option>
                        <option value="Đà Lạt">Đà Lạt</option>
                        <option value="Vũng Tàu">Vũng Tàu</option>
                        <option value="Nha Trang">Nha Trang</option>
                    </select>
                    <div style="width: 20%;display: flex; align-items: center;">
                        <span style="font-size: 19px;">from: </span> <input type="date" required="" id="checkin"  style="display: inline-block;" name="checkin" onchange="checkdate(this);" class="form-control"><!-- comment -->
                    </div>
                    <div style="width: 20%;display: flex; align-items: center;">
                        <span style="font-size: 19px;">to: </span> <input type="date" required style="display: inline-block;" name="checkout"  onchange="checkoutdate(this);" class="form-control"><!-- comment -->
                    </div>
                    <input type="number" name="guest" class="form-control" min="0" required placeholder="number people" style="width: 15%"><!-- comment -->
                    <select name="star" class="form-select" style="width: 10%">
                        <option value="">All</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <select name="sort" class="form-select" style="width: 10%">
                        <option value="asc">Giá từ thấp đến cao</option>
                        <option value="desc">Giá từ cao đến thấp</option>
                    </select>
                    <button type="submit" class="btn btn-primary">Find</button>
                </form>
                <h1 style="text-align: center">Available Hotels for you</h1>

                <c:forEach var="h" items="${hl}">
                    <div style="margin-bottom: 10px;border: 1px solid lightgray; border-radius: 10px;display: flex; justify-content: space-between;padding: 8px; " >
                        <img width="30%" style="height: 225px;object-fit: cover;"  src="${h.banner}" alt="alt"/>
                        <div width="35%">
                            <h2 style="color: #636ce4">${h.name}</h2>
                            <p>Address: ${h.address}</p>
                            <p><i class="fa fa-bar-chart" aria-hidden="true"></i> Best price</p>
                            <p><i class="fa fa-car" aria-hidden="true"></i> Free taking</p>
                            <p style="color: green; font-size: 11px">FREE cancellation<br>
                                No prepayment needed – pay at the property</p>
                        </div>
                        <div width="33%">
                            <h3>Range Price:<br> ${h.lowestPrice} VND <br> - <br> ${h.highestPrice} VND</h3>
                            <p>Includes taxes and charges</p>
                            <a href="DetailHotel?hid=${h.id}&pro=${param['pro']}&checkin=${param['checkin']}&checkout=${param['checkout']}&guest=${param['guest']}&star=${param['star']}" class="btn btn-primary" >See detail </a>
                        </div>
                    </div>
                </c:forEach>

                <div style="display: flex; justify-content: center;">
                    <ul class="pagination" >
                        <li  class="page-item next"><a href="FindHotel?index=1&pro=${param['pro']}&checkin=${param['checkin']}&checkout=${param['checkout']}&guest=${param['guest']}&star=${param['star']}"><i class="fa fa-angle-left" class="page-link" aria-hidden="true"></i></a></li>
                        <c:forEach var = "i" begin = "1" end = "${numberPage}"> <!-- phân trang bằng sql -->
                            <li class="${param['index']==i?'page-item active':'page-item'}"><a href="FindHotel?index=${i}&pro=${param['pro']}&checkin=${param['checkin']}&checkout=${param['checkout']}&guest=${param['guest']}&star=${param['star']}"   class="page-link">${i}</a></li>
                            </c:forEach>
                        <li  class="page-item next"><a href="FindHotel?index=${numberPage}&pro=${param['pro']}&checkin=${param['checkin']}&checkout=${param['checkout']}&guest=${param['guest']}&star=${param['star']}"><i class="fa fa-angle-right" class="page-link" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <jsp:include page="footer.jsp"/>



        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>

        <script src="assets/js/jquery-3.4.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.countdown.min.js"></script>
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <script src="assets/js/jquery.nicescroll.min.js"></script>
        <script src="assets/js/slick.min.js"></script>
        <script src="assets/js/biolife.framework.js"></script>
        <script src="assets/js/functions.js"></script>
        <script>
                            var date = new Date();
                            var currentDate = date.toISOString().slice(0, 10);
                            document.getElementById('checkin').value = currentDate;
        </script>
        <script>
            function checkdate(x) {
                var inputdate = new Date(x.value);
                var fixdate = inputdate.toISOString().slice(0, 10);
                var date = new Date();
                var currentDate = date.toISOString().slice(0, 10);  //Chuyển đổi ngày hiện tại thành định dạng ISO 8601 và chỉ lấy phần đầu (10 ký tự) đại diện cho ngày.
                if (fixdate < currentDate) {
                    alert("Check in date must be after current day");
                    x.value = currentDate;
                } else {
                    x.value = fixdate;

                }
            }
        </script>
        <script>
            function checkoutdate(x) {
                var date1 = new Date(document.getElementById('checkin').value);
                var date2 = new Date(x.value);
                var checkindate = date1.toISOString().slice(0, 10);
                var inputdate = date2.toISOString().slice(0, 10);
                var date = new Date();
                var currentDate = date.toISOString().slice(0, 10);
                if (inputdate < currentDate) {
                    alert("Check in date must be after current day");
                    x.value = currentDate;
                } else if (checkindate > inputdate) {
                    alert("Check out date must be after checkin date");
                    x.value = "";

                } else {
                    x.value = inputdate;
                }
            }
        </script>
    </body>

</html>