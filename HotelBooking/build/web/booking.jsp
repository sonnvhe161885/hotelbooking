<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Hotel</title>

        <link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/nice-select.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main-color.css">


    </head>

    <body class="biolife-body">

        <jsp:include page="header.jsp"/>

        <!-- Page Contain -->
        <div class="page-contain">
            <div id="main-content" class="main-content container">
                <h1>Detail information about hotel</h1>
                <div style="margin-bottom: 10px;border: 1px solid lightskyblue; background-color:#87cefa5e ; border-radius: 10px;display: flex; justify-content: space-between;padding: 10px; " >
                    <img width="30%" style="height: 225px;object-fit: cover;"  src="${h.banner}" alt="alt"/>
                    <div width="35%">
                        <h2 style="color: #636ce4">${h.name}</h2>
                        <p>Address: ${h.address}</p>
                        <p><i class="fa fa-bar-chart" aria-hidden="true"></i> Best price</p>
                        <p><i class="fa fa-car" aria-hidden="true"></i> Free taking</p>
                        <p style="color: green; font-size: 11px">FREE cancellation<br>
                            No prepayment needed – pay at the property</p>
                    </div>
                    <div width="33%">
                        <h3> Price: ${param.price} VND</h3>
                        <p>Includes taxes and charges</p>
                    </div>
                </div>

                <div style="margin-bottom: 10px;border: 1px solid lightskyblue; border-radius: 10px;padding: 8px;float: left; width: 30% " >
                    <h2>Booking detail</h2>
                    <div style="width: 100%;padding: 15px  ">
                        <div>
                            Check-in: <br> <b>${param.checkin}</b>
                        </div>
                        <div>
                            Check-out: <br> <b>${param.checkout}</b>
                        </div>
                        <div>
                            Total People: <br> <b>${param.guest}</b>
                        </div>
                        <div>
                            Room: <br> <b>${param.rtype}</b>
                        </div>
                    </div>
                </div>
                <div style="margin-bottom: 10px;border: 1px solid lightskyblue;  border-radius: 10px;padding: 8px;float: right; width: 66% " >
                    <h2>Enter your details</h2>   
                    <form method="post" action="BookingRoom">
                        <input type="hidden" class="form-control" value="${param.checkin}" name="checkin">
                        <input type="hidden" class="form-control" value="${param.checkout}" name="checkout">
                        <input type="hidden" class="form-control" value="${param.price}" name="price">
                        <input type="hidden" class="form-control" value="${param.rid}" name="rid">
                        <input type="hidden" class="form-control" value="${param.guest}" name="guest">


                        <label>Full Name</label>
                        <c:if test="${sessionScope['account']!=null}">
                            <input type="text" class="form-control"  required=""value="${sessionScope['account'].getName()}" name="fullname">
                        </c:if>
                        <c:if test="${sessionScope['account']==null}">
                            <input type="text" class="form-control"  required=""value="${sessionScope['account'].getName()}" name="fullname">
                        </c:if>



                        <label>Email</label>
                        <c:if test="${sessionScope['account']!=null}">
                            <input type="text" class="form-control"  required=""value="${sessionScope['account'].getEmail()}" name="email">
                        </c:if>
                        <c:if test="${sessionScope['account']==null}">
                            <input type="email" class="form-control" required  name="email">
                        </c:if>

                        <label>Notes</label>
                        <input type="text" class="form-control" value="" name="notes"><!-- comment -->
                        <button style="margin-top: 8px;" type="submit" class="btn btn-info">Book</button>
                    </form>
                </div>
            </div>
        </div>
        <jsp:include page="footer.jsp"/>



        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>

        <script src="assets/js/jquery-3.4.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.countdown.min.js"></script>
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <script src="assets/js/jquery.nicescroll.min.js"></script>
        <script src="assets/js/slick.min.js"></script>
        <script src="assets/js/biolife.framework.js"></script>
        <script src="assets/js/functions.js"></script>
    </body>

</html>