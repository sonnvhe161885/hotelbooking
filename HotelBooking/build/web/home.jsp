<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Hotel</title>

        <link href="https://fonts.googleapis.com/css?family=Cairo:400,600,700&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins:600&amp;display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400i,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Ubuntu&amp;display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/nice-select.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slick.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/main-color.css">


    </head>

    <body class="biolife-body">

        <jsp:include page="header.jsp"/>

        <!-- Page Contain -->
        <div class="page-contain">
            <div id="main-content" class="main-content container">
                <img src="https://img.freepik.com/premium-psd/traveling-social-media-banner-facebook-cover-template-premium-psd_632100-210.jpg" width="100%"  alt="alt"/>
                <form action="FindHotel" style="display: flex; justify-content: space-between; " method="">
                    <!--<input type="text" placeholder="Address" name="pro" style="width: 20%" class="form-control"> comment -->
                    <select name="pro" class="form-select" style="width: 10%">
                        <option value="">Address</option>
                        <option value="Hà Nội">Hà Nội</option>
                        <option value="Đà Nẵng">Đà Nẵng</option>
                        <option value="Đà Lạt">Đà Lạt</option>
                        <option value="Vũng Tàu">Vũng Tàu</option>
                        <option value="Nha Trang">Nha Trang</option>
                    </select>
                    <div style="width: 20%;display: flex; align-items: center;">
                        <span style="font-size: 19px;">from: </span> <input type="date" required="" id="checkin"  style="display: inline-block;" name="checkin" onchange="checkdate(this);" class="form-control"><!-- comment -->
                    </div>
                    <div style="width: 20%;display: flex; align-items: center;">
                        <span style="font-size: 19px;">to: </span> <input type="date" required style="display: inline-block;" name="checkout"  onchange="checkoutdate(this);" class="form-control"><!-- comment -->
                    </div>
                    <input type="number" name="guest" class="form-control" min="0" required placeholder="number people" style="width: 15%"><!-- comment -->
                    <select name="star" class="form-select" style="width: 10%">
                        <option value="">All</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <select name="sort" class="form-select" style="width: 10%">
                        <option value="asc">Giá từ thấp đến cao</option>
                        <option value="desc">Giá từ cao đến thấp</option>
                    </select>
                    <button type="submit" class="btn btn-primary">Find</button>
                </form>
                <h2>Trending destinations</h2>
                <p>Travellers searching for Vietnam also booked these</p>
                <div class="row">
                    <div class="col-md-6" >
                        <h2 style="text-align: center;position: relative; top: 55px;height: 10px;"><a style=" color: white; " href="http://localhost:9999/HotelBooking/FindHotel?pro=%C4%90%C3%A0+N%E1%BA%B5ng&checkin=2023-07-20&checkout=2023-07-21&guest=1&star=&sort=asc">Da Nang <i class="fa fa-plane" aria-hidden="true"></i></a></h2>
                        <img style="border-radius: 10px;width: 100%; height: 270px" src="https://cf.bstatic.com/xdata/images/city/600x600/688831.jpg?k=7b999c7babe3487598fc4dd89365db2c4778827eac8cb2a47d48505c97959a78&o=" alt="alt"/>
                    </div>
                    <div class="col-md-6" >
                        <h2 style="text-align: center;position: relative; top: 55px;height: 10px;"><a style=" color: white; " href="http://localhost:9999/HotelBooking/FindHotel?pro=V%C5%A9ng+T%C3%A0u&checkin=2023-07-21&checkout=2023-07-22&guest=1&star=&sort=asc">Sai Gon <i class="fa fa-plane" aria-hidden="true"></i></a></h2>
                        <img style="border-radius: 10px;width: 100%; height: 270px" src="https://cf.bstatic.com/xdata/images/city/600x600/688893.jpg?k=d32ef7ff94e5d02b90908214fb2476185b62339549a1bd7544612bdac51fda31&o=" alt="alt"/>
                    </div>
                    <div class="col-md-4" >
                        <h2 style="text-align: center;position: relative; top: 55px;height: 10px;"><a style=" color: white; " href="http://localhost:9999/HotelBooking/FindHotel?pro=H%C3%A0+N%E1%BB%99i&checkin=2023-07-20&checkout=2023-07-21&guest=1&star=&sort=asc">Ha Noi<i class="fa fa-plane" aria-hidden="true"></i></a></h2>
                        <img style="border-radius: 10px;width: 100%; height: 270px" src="https://cf.bstatic.com/xdata/images/city/600x600/688907.jpg?k=8a219233969467d9f7ff828918cce2a53b4db6f1da1039d27222441ffb97c409&o=" alt="alt"/>
                    </div>
                    <div class="col-md-4" >
                        <h2 style="text-align: center;position: relative; top: 55px;height: 10px;"><a style=" color: white; " href="http://localhost:9999/HotelBooking/FindHotel?pro=Nha+Trang&checkin=2023-07-22&checkout=2023-07-28&guest=1&star=&sort=asc">Nha Trang <i class="fa fa-plane" aria-hidden="true"></i></a></h2>
                        <img style="border-radius: 10px;width: 100%; height: 270px" src="https://cf.bstatic.com/xdata/images/city/600x600/688866.jpg?k=fc9d2cb9fe2f6d1160e10542cd2b83f5a8008401d33e8750ee3c2691cf4d4f7e&o=" alt="alt"/>
                    </div>
                    <div class="col-md-4" >
                        <h2 style="text-align: center;position: relative; top: 55px;height: 10px;"><a style=" color: white; " href="http://localhost:9999/HotelBooking/FindHotel?pro=%C4%90%C3%A0+L%E1%BA%A1t&checkin=2023-07-20&checkout=2023-07-21&guest=1&star=&sort=asc">Da Lat <i class="fa fa-plane" aria-hidden="true"></i></a></h2>
                        <img style="border-radius: 10px;width: 100%; height: 270px" src="https://cf.bstatic.com/xdata/images/city/600x600/688844.jpg?k=02892d4252c5e4272ca29db5faf12104004f81d13ff9db724371de0c526e1e15&o=" alt="alt"/>
                    </div>
                </div>

            </div>
        </div>
        <jsp:include page="footer.jsp"/>



        <!-- Scroll Top Button -->
        <a class="btn-scroll-top"><i class="biolife-icon icon-left-arrow"></i></a>

        <script src="assets/js/jquery-3.4.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.countdown.min.js"></script>
        <script src="assets/js/jquery.nice-select.min.js"></script>
        <script src="assets/js/jquery.nicescroll.min.js"></script>
        <script src="assets/js/slick.min.js"></script>
        <script src="assets/js/biolife.framework.js"></script>
        <script src="assets/js/functions.js"></script>
        <script>
                            var date = new Date();
                            var currentDate = date.toISOString().slice(0, 10);
                            document.getElementById('checkin').value = currentDate;
        </script>
        <script>
            function checkdate(x) {
                var inputdate = new Date(x.value);
                var fixdate = inputdate.toISOString().slice(0, 10);
                var date = new Date();
                var currentDate = date.toISOString().slice(0, 10);
                if (fixdate < currentDate) {
                    alert("Check in date must be after current day");
                    x.value = currentDate;
                } else {
                    x.value = fixdate;

                }
            }
        </script>
        <script>
            function checkoutdate(x) {
                var date1 = new Date(document.getElementById('checkin').value);
                var date2 = new Date(x.value);
                var checkindate = date1.toISOString().slice(0, 10);
                var inputdate = date2.toISOString().slice(0, 10);
                var date = new Date();
                var currentDate = date.toISOString().slice(0, 10);
                if (inputdate < currentDate) {
                    alert("Check in date must be after current day");
                    x.value = currentDate;
                } else if (checkindate > inputdate) {
                    alert("Check out date must be after checkin date");
                    x.value = "";

                } else {
                    x.value = inputdate;
                }
            }
        </script>
    </body>

</html>