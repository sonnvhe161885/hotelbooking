<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!-- HEADER -->
<nav class="navbar navbar-default">
    <div class="container">



        <!-- COLLAPSIBLE NAVBAR -->
        <div class="collapse navbar-collapse" id="alignment-example">

            <!-- Links -->
            <ul class="nav navbar-nav">
                <li >
                    <a href="HomePage" class="nav-link" style="font-size: 20px;">Home</a>
                </li>
                <li >
                    <c:if test="${sessionScope['account'].getRoles().getId() ==2}">
                    <li ><a href="ManagerUser" >Manager Page</a></li>
                    </c:if>
                </li>
            </ul>

            <!-- Search -->


            <ul class="nav navbar-nav navbar-right">

                <c:if test="${sessionScope['account']!=null}">
                    <li>    <a href="MyOrder" >My order</a>
                    </li>
                    <li ><a href="Profile" style="color: black;" class="login-link" ><i class="biolife-icon icon-login"></i>  Profile</a></li>
                    </c:if>
                    <c:if test="${sessionScope['account']==null}">
                    <li ><a href="login" style="color: black;" class="login-link" style="color: black;"><i class="biolife-icon icon-login"></i>Login/Register</a></li>
                    </c:if>
                    <c:if test="${sessionScope['account']!=null}">
                    <li ><a href="Logout" style="color: black;" class="login-link" ><i class="fa fa-sign-out" style="font-size: 18px; margin-left: 10px;" aria-hidden="true"> </i> </a></li>
                    </c:if>
            </ul>
        </div>

    </div>
</nav>